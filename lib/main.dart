import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task_flutter/data/datasource/remote_datasource.dart';
import 'package:task_flutter/pages/bloc/employee_bloc.dart';
import 'package:task_flutter/pages/form_add.dart';
import 'package:task_flutter/pages/home.dart';
import 'package:task_flutter/pages/splash_screen.dart';

void main() {
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return 
    
    MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => EmployeeBloc(remoteDataSource: RemoteDataSource())..add(LoadEmployee()),
        ),
      ],
      child: 
      MaterialApp(
        initialRoute: '/',
        routes: {
          '/': (context) => const SplashScreen(),
          '/home': (context) => const Home(),
          '/form_add': (context) => const FormAdd(),
        },
      ),
    );
  }
}