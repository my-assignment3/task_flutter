import 'package:dio/dio.dart';
import 'package:task_flutter/data/model/employee.dart';

class RemoteDataSource {
  final dio = Dio(BaseOptions(
      baseUrl: 'https://tiraapi-dev.tigaraksa.co.id/tes-programer-mobile'));

  Future<DataEmployee> getEmployees() async {
    final response = await dio.get('/api/karyawan/all');
    final result = DataEmployee.fromJson(response.data);
    return result;
  }

 Future<void> insertEmployee({
    required int nik,
    required String firstName,
    required String lastName,
    required String alamat,
    required bool aktif,
  }) async {
    try {
      await dio.post('/karyawan/insert', data: [{
        'nik': nik,
        'first_name': firstName,
        'last_name': lastName,
        'alamat': alamat,
        'aktif': aktif,
      }]);

    } catch (error) {
      throw Exception('Failed to insert employee: $error');
    }
  }
}
