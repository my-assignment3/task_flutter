import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task_flutter/pages/bloc/employee_bloc.dart';

class FormAdd extends StatefulWidget {
  const FormAdd({Key? key}) : super(key: key);

  @override
  State<FormAdd> createState() => _FormAddState();
}

class _FormAddState extends State<FormAdd> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _nikController = TextEditingController();
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _alamatController = TextEditingController();
  bool _aktif = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Form Add Employee',
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.blue,
      ),
      body: BlocListener<EmployeeBloc, EmployeeState>(
        listener: (context, state) {
          if (state is EmployeeAdded) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text('Employee added successfully!')),
            );

            Navigator.pushNamed(context, '/home');
          } else if (state is EmployeeError) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text('Failed to add employee: ${state.error}')),
            );
          }
        },
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                  controller: _nikController,
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    // FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                    FilteringTextInputFormatter.digitsOnly
                  ],
                  decoration: InputDecoration(labelText: 'NIK'),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter NIK';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: _firstNameController,
                  decoration: InputDecoration(labelText: 'First Name'),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter first name';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: _lastNameController,
                  decoration: InputDecoration(labelText: 'Last Name'),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter last name';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: _alamatController,
                  decoration: InputDecoration(labelText: 'Alamat'),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter alamat';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 16.0),
                const Text("Status:"),
                ListTile(
                  title: const Text('Aktif'),
                  leading: Radio<bool>(
                    value: true,
                    groupValue: _aktif,
                    onChanged: (bool? value) {
                      setState(() {
                        _aktif = value ?? false;
                      });
                    },
                  ),
                ),
                ListTile(
                  title: const Text('Nonaktif'),
                  leading: Radio<bool>(
                    value: false,
                    groupValue: _aktif,
                    onChanged: (bool? value) {
                      setState(() {
                        _aktif = value ?? false;
                      });
                    },
                  ),
                ),
                const SizedBox(height: 20),
                SizedBox(
                  width: double.infinity,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: TextButton(
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          _submitForm(context);
                        }
                      },
                      style: TextButton.styleFrom(
                        backgroundColor: Colors.blue,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(0),
                        ),
                      ),
                      child: const Text(
                        "Add",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _submitForm(BuildContext context) {
    final employeeBloc = BlocProvider.of<EmployeeBloc>(context);

    employeeBloc.add(InsertEmployee(
      nik: int.parse(_nikController.text),
      firstName: _firstNameController.text,
      lastName: _lastNameController.text,
      alamat: _alamatController.text,
      aktif: _aktif,
    ));
  }
}
