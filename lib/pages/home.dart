import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task_flutter/pages/bloc/employee_bloc.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  void initState() {
    super.initState();
     context.read<EmployeeBloc>().add(LoadEmployee());
  }
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
        appBar: AppBar(
          title: const Text('Employees', style: TextStyle(color: Colors.white)),
          backgroundColor: Colors.blue,
        ),
        body: BlocBuilder<EmployeeBloc, EmployeeState>(
          builder: (context, state) {
            if (state is EmployeeLoading) {
              return const Center(child: CircularProgressIndicator());
            } else if (state is EmployeeLoaded) {
              final data = state.employees;
              return Column(
                children: <Widget>[
                  const SizedBox(
                    height: 20,
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: TextButton(
                        onPressed: () {
                          Navigator.pushNamed(context, '/form_add');
                        },
                        style: TextButton.styleFrom(
                          backgroundColor: Colors.blue,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0),
                          ),
                        ),
                        child: const Text(
                          "Add Employee",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Expanded(
                    child: ListView.builder(
                      itemCount: data.length,
                      itemBuilder: (context, index) {
                        final employee = data[index];

                        return ListTile(
                          leading: Text("${index + 1}"),
                          title: Text(
                              '${employee.firstName} ${employee.lastName}'),
                          subtitle: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(employee.alamat ?? ''),
                              Text("${employee.nik}"),
                              Row(
                                children: [
                                  Text(employee.aktif ? 'Aktif' : 'Nonaktif'),
                                  const SizedBox(width: 8),
                                  Icon(
                                    Icons.square,
                                    color: employee.aktif
                                        ? Colors.green
                                        : Colors.red,
                                    size: 16,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                ],
              );
            } else if (state is EmployeeError) {
              return Center(child: Text(state.error));
            } else {
              return const Center(child: Text('No employees found'));
            }
          },
        ),
      // ),
    );
  }
}
