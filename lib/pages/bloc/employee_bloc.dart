import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task_flutter/data/datasource/remote_datasource.dart';
import 'package:task_flutter/data/model/employee.dart';

part 'employee_event.dart';
part 'employee_state.dart';

class EmployeeBloc extends Bloc<EmployeeEvent, EmployeeState> {
  final RemoteDataSource remoteDataSource;

  EmployeeBloc({required this.remoteDataSource}) : super(EmployeeInitial()) {
    on<LoadEmployee>((event, emit) async {
      emit(EmployeeLoading());

      try {
        final result = await remoteDataSource.getEmployees();
        emit(EmployeeLoaded(result.values));
      } catch (error) {
        print("Error load employee: $error");
        emit(EmployeeError(error?.toString() ?? "Unknown error occurred"));
      }
    });

    on<InsertEmployee>((event, emit) async {
      emit(EmployeeLoading());

      try {
        await remoteDataSource.insertEmployee(
          nik: event.nik,
          firstName: event.firstName,
          lastName: event.lastName,
          alamat: event.alamat,
          aktif: event.aktif,
        );
        emit(EmployeeAdded());
      } catch (error) {
        print("Error inserting employee: $error");
        emit(EmployeeError(error?.toString() ?? "Unknown error occurred"));
      }
    });
  }
}
