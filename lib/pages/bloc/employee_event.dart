part of 'employee_bloc.dart';

@immutable
sealed class EmployeeEvent {}

final class LoadEmployee extends EmployeeEvent {}

class InsertEmployee extends EmployeeEvent {
  final int nik;
  final String firstName;
  final String lastName;
  final String alamat;
  final bool aktif;

  InsertEmployee({
    required this.nik,
    required this.firstName,
    required this.lastName,
    required this.alamat,
    required this.aktif,
  });
}