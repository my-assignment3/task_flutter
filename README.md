# SIMPLE FLUTTER PROJECT
This projects is only display and insert data. There are screens inside this app such as:

- Splash screen.
- Home.
- Form to add data.

## Installation

1. **You can clone the repository**:
```bash
git clone repository -->link repository<--
```
2. **Make sure you should keep update the package to able run these project**:
```bash
flutter pub get
```
3. **You can run this project**:
```bash
flutter pub run 
```

## Build APK

1. **You can follow this command**:

Production:
```bash
flutter build apk --release
```

Development:
```bash
flutter build apk --debug
```

## Packages installed
- flutter_bloc.
- dio.

## Tools
The tools that used for support develop this project such as:

- Flutter 3.16.9 version.
- Dart 3.2.6 version.
- Android Studio to run emulator.
- Google Browser